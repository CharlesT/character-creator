import "./App.css";
import CharacterSheet from "./pages/CharacterSheet";
import character_data from "./characters/My_Character.json";
import { Navigate, Route, Routes } from "react-router-dom";
import AppLayout from "./components/AppLayout";
import Cultivation from "./pages/Cultivation";
import Class from "./pages/Class";
import Skills from "./pages/Skills";
import BonusPerks from "./pages/BonusPerks";
import Titles from "./pages/Titles";
import jsonToCharacter from "./characters/fileReader";

// TODO: Ideally find a way to smoothly animate transitions. The recent release of
// react-router v6 means it's been hard to get examples, so I'm putting this off for
// you, future me :D
function App() {
    const app_data = jsonToCharacter(character_data);

    return (
        <Routes>
            <Route path="/" element={<AppLayout />}>
                <Route index element={<Navigate to="/summary" />} />
                <Route
                    path="/summary"
                    element={<CharacterSheet summary={app_data.summary} />}
                />
                <Route path="/class" element={<Class {...app_data.class} />} />
                <Route
                    path="/cultivation"
                    element={<Cultivation {...app_data.cult} />}
                />
                <Route path="/skills" element={<Skills {...app_data.skill} />} />
                <Route
                    path="/bonusperks"
                    element={<BonusPerks perks={app_data.bonusPerks} />}
                />
                <Route path="/titles" element={<Titles titles={app_data.titles} />} />
                <Route
                    path="*"
                    element={
                        <p>
                            Sorry, but that's a bad link! Use the navigation menu to reach a
                            different page.
                        </p>
                    }
                />
            </Route>
        </Routes>
    );
}

export default App;

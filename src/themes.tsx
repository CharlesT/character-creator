import { css } from "styled-components";
import { teal, tealDark, sage, sageDark } from "@radix-ui/colors";

const lightTeal = css`
  --teal-app-bg: ${teal.teal1};
  --teal-subtle-bg: ${teal.teal2};
  --teal-element-bg: ${teal.teal3};
  --teal-hovered-element-bg: ${teal.teal4};
  --teal-active-element-bg: ${teal.teal5};
  --teal-subtle-border: ${teal.teal6};
  --teal-element-border: ${teal.teal7};
  --teal-hovered-element-border: ${teal.teal8};
  --teal-solid-bg: #29a1a0;
  --teal-hovered-solid-bg: ${teal.teal10};
  --teal-text-low-contrast: ${teal.teal11};
  /* Made this white since I felt black didn't look quite right.*/
  --teal-text-high-contrast: ${tealDark.teal12};
`;

const darkTeal = css`
  --teal-app-bg: ${tealDark.teal1};
  --teal-subtle-bg: ${tealDark.teal2};
  --teal-element-bg: ${tealDark.teal3};
  --teal-hovered-element-bg: ${tealDark.teal4};
  --teal-active-element-bg: ${tealDark.teal5};
  --teal-subtle-border: ${tealDark.teal6};
  --teal-element-border: ${tealDark.teal7};
  --teal-hovered-element-border: ${tealDark.teal8};
  --teal-solid-bg: #29a1a0;
  --teal-hovered-solid-bg: ${tealDark.teal10};
  --teal-text-low-contrast: ${tealDark.teal11};
  --teal-text-high-contrast: ${tealDark.teal12};
`;

const lightGrey = css`
  --grey-app-bg: ${sage.sage1};
  --grey-subtle-bg: ${sage.sage2};
  --grey-element-bg: ${sage.sage3};
  --grey-hovered-element-bg: ${sage.sage4};
  --grey-active-element-bg: ${sage.sage5};
  --grey-subtle-border: ${sage.sage6};
  --grey-element-border: ${sage.sage7};
  --grey-hovered-element-border: ${sage.sage8};
  --grey-solid-bg: ${sage.sage9};
  --grey-hovered-solid-bg: ${sage.sage10};
  --grey-text-low-contrast: ${sage.sage11};
  --grey-text-high-contrast: ${sage.sage12};
`;

const darkGrey = css`
  --grey-app-bg: ${sageDark.sage1};
  --grey-subtle-bg: ${sageDark.sage2};
  --grey-element-bg: ${sageDark.sage3};
  --grey-hovered-element-bg: ${sageDark.sage4};
  --grey-active-element-bg: ${sageDark.sage5};
  --grey-subtle-border: ${sageDark.sage6};
  --grey-element-border: ${sageDark.sage7};
  --grey-hovered-element-border: ${sageDark.sage8};
  --grey-solid-bg: ${sageDark.sage9};
  --grey-hovered-solid-bg: ${sageDark.sage10};
  --grey-text-low-contrast: ${sageDark.sage11};
  --grey-text-high-contrast: ${sageDark.sage12};
`;

const lightShadows = css`
  --content-area-box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.25);
  --strong-box-shadow: rgba(0, 0, 0, 0.07) 0px 1px 2px,
    rgba(0, 0, 0, 0.07) 0px 2px 4px, rgba(0, 0, 0, 0.07) 0px 4px 8px,
    rgba(0, 0, 0, 0.07) 0px 8px 16px, rgba(0, 0, 0, 0.07) 0px 16px 32px,
    rgba(0, 0, 0, 0.07) 0px 32px 64px;
  --weak-box-shadow: rgba(0, 0, 0, 0.25) 0 0 10px;
  --drop-shadow: drop-shadow(0 0 20px rgba(0, 0, 0, 0.5));
`;

const darkShadows = css`
  --content-area-box-shadow: 0 0px 15px rgba(0, 0, 0, 1);
  --strong-box-shadow: rgba(0, 0, 0, 0.9) 0px 1px 2px,
    rgba(0, 0, 0, 0.9) 0px 2px 4px, rgba(0, 0, 0, 0.9) 0px 4px 8px,
    rgba(0, 0, 0, 0.9) 0px 8px 16px;
  --weak-box-shadow: rgba(0, 0, 0, 0.1) 0 0 3px 0;
  --drop-shadow: drop-shadow(0 0 20px rgba(0, 0, 0, 1));
`;

export const themes = css`
  .dark-theme {
    ${darkTeal};
    ${darkGrey};
    ${darkShadows};
  }
  .light-theme {
    ${lightTeal};
    ${lightGrey};
    ${lightShadows};
  }
`;

export const colors = {
  teal: {
    appBg: "var(--teal-app-bg)",
    subtleBg: "var(--teal-subtle-bg)",
    elementBg: "var(--teal-element-bg)",
    hoveredElementBg: "var(--teal-hovered-element-bg)",
    activeElementBg: "var(--teal-active-element-bg)",
    subtleBorder: "var(--teal-subtle-border)",
    elementBorder: "var(--teal-element-border)",
    hoveredElementBorder: "var(--teal-hovered-element-border)",
    solidBg: "var(--teal-solid-bg)",
    hoveredSolidBg: "var(--teal-hovered-solid-bg)",
    textLowContrast: "var(--teal-text-low-contrast)",
    textHighContrast: "var(--teal-text-high-contrast)",
  },
  grey: {
    appBg: "var(--grey-app-bg)",
    subtleBg: "var(--grey-subtle-bg)",
    elementBg: "var(--grey-element-bg)",
    hoveredElementBg: "var(--grey-hovered-element-bg)",
    activeElementBg: "var(--grey-active-element-bg)",
    subtleBorder: "var(--grey-subtle-border)",
    elementBorder: "var(--grey-element-border)",
    hoveredElementBorder: "var(--grey-hovered-element-border)",
    solidBg: "var(--grey-solid-bg)",
    hoveredSolidBg: "var(--grey-hovered-solid-bg)",
    textLowContrast: "var(--grey-text-low-contrast)",
    textHighContrast: "var(--grey-text-high-contrast)",
  },
  rarity: {
    t1: "#fdfefe",
    t2: "#27AE60",
    t3: "#2471A3",
    t4: "#7D3C98",
    t5: "#f1c40f",
    t6: "#D35400",
    t7: "#7B241C",
    t8: "#B3CA1F",
    t9: "#dc2367",
  },
  shadows: {
    contentArea: "var(--content-area-box-shadow)",
    strong: "var(--strong-box-shadow)",
    weak: "var(--weak-box-shadow)",
    dropShadow: "var(--drop-shadow)",
  },
};

import PerkTable, { Perk } from "../components/PerkTable";

export default function Titles(props: { titles: Perk[] }) {
  return (
    <PerkTable
      headings={{ type: "Type" }}
      perks={props.titles}
      perkTypes={{
        strict: [],
        optional: ["General"],
      }}
    />
  );
}

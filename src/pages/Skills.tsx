import { Skill } from "../components/EditSkillForm";
import PerkTable, { Perk } from "../components/PerkTable";
import SkillSheet from "../components/SkillSheet";

interface SkillsProps {
  passive: Skill[];
  active: Skill[];
  perks: Perk[];
}

export default function Skills(props: SkillsProps) {
  return (
    <>
      <SkillSheet passives={props.passive} actives={props.active} />
      <PerkTable
        headings={{ type: "Type" }}
        perks={props.perks}
        perkTypes={{
          strict: [],
          optional: ["Active", "Passive"],
        }}
      />
    </>
  );
}

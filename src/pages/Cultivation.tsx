import Card from "../components/basics/Card";
import { Path, PathDisplay } from "../components/PathDisplay";
import PerkTable, { Perk } from "../components/PerkTable";
import { ConduitSpeed, CoreSize, Stage, stageValues } from "../enums";
import * as G from "../styles/generalStyles";

interface CultivationProps {
  primaryPath: Path;
  secondPath: Path;
  aspect1: string;
  aspect2?: string; // Optional, only introduced once the cultivator reaches ascended realm.
  coreSize: CoreSize;
  conduitSpeed: ConduitSpeed;
  bonus: number; // Cultivation Bonus, max 90 (%).
  perks: Perk[];
}

export default function Cultivation(props: CultivationProps) {
  return (
    <G.DataArea>
      <G.HFlex>
        <Card>
          <PathDisplay {...props.primaryPath} />
        </Card>
        <div style={{ width: "2em" }} />
        <Card>
          <PathDisplay {...props.secondPath} />
        </Card>
      </G.HFlex>
      <PerkTable
        headings={{}}
        perks={props.perks}
        perkTypes={{
          strict: stagesWithPerks.map((val) => val.toString()),
          optional: ["Aspect Perk", "Second Aspect Perk"],
        }}
      />
    </G.DataArea>
  );
}

const stagesWithoutPerks: Stage[] = [
  "Early Mortal",
  "Mid Mortal",
  "Peak Mortal",
  "Peak Lord",
  "Early Ascended",
];

const stagesWithPerks: Stage[] = [...stageValues].filter(
  (stage) => !stagesWithoutPerks.includes(stage)
);

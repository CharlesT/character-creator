import CharacterSummary, { CharProps } from "../components/CharacterSummary";

interface SheetProps {
  summary: CharProps;
}

export default function CharacterSheet(props: SheetProps) {
  return <CharacterSummary {...props.summary}></CharacterSummary>;
}

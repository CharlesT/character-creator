import PerkTable, { Perk } from "../components/PerkTable";

export default function BonusPerks(props: { perks: Perk[] }) {
  return (
    <PerkTable
      headings={{ type: "Type" }}
      perks={props.perks}
      perkTypes={{
        strict: [],
        optional: ["Thing 1"],
      }}
    />
  );
}

import Card from "../components/basics/Card";
import NumericInput from "../components/basics/inputs/NumericInput";
import TextInput from "../components/basics/inputs/TextInput";
import Select from "../components/basics/menus/Select";
import PerkTable, { Perk } from "../components/PerkTable";
import TextEditor from "../components/TextEditor";
import { Attribute, attributeValues, Rarity, rarityValues } from "../enums";
import * as G from "../styles/generalStyles";

interface ClassProps {
  summary: ClassSummaryProps;
  abilities: Abilities;
  attunements: Perk[];
  perks: Perk[];
}

export default function Class(props: ClassProps) {
  return (
    <G.DataArea>
      <Card>
        <G.HFlex>
          <ClassSummary {...props.summary} />
          <AbilitiesDisplay {...props.abilities} />
        </G.HFlex>
      </Card>
      <AttunementDisplay attunements={props.attunements} />
      <PerkTable
        headings={{ type: "Level" }}
        perks={props.perks}
        perkTypes={{
          optional: [],
          strict: Array((9 * 60) / 15)
            .fill(0)
            .map((_, i) => `Level ${(i + 1) * 15}`),
        }}
      />
    </G.DataArea>
  );
}

interface ClassSummaryProps {
  name: string;
  level: number;
  rarity: Rarity;
  description: string;
  attrPrimary: Attribute;
  attrSecondary: Attribute;
}

const ClassSummary = (props: ClassSummaryProps) => (
  <G.VFlex>
    <TextInput value={props.name} large />
    <NumericInput value={props.level} setVal={() => { }} />
    <Select
      values={rarityValues.map((val) => ({ name: val, description: "" }))}
      currName={props.rarity}
      onSelect={() => { }}
    />
    <TextEditor
      value={props.description}
      placeholder="What's the theme of the class?"
    />
    <Select
      values={attributeValues.map((val) => ({ name: val, description: "" }))}
      currName={props.attrPrimary}
      onSelect={() => { }}
    />
    <Select
      values={attributeValues.map((val) => ({ name: val, description: "" }))}
      currName={props.attrSecondary}
      onSelect={() => { }}
    />
  </G.VFlex>
);

interface Ability {
  name: string;
  description: string;
}

interface Abilities {
  combat: Ability;
  movement: Ability;
  support: Ability;
}

const AbilityInput = (props: Ability) => (
  <G.HFlex>
    <TextInput value={props.name} />
    <TextEditor
      value={props.description}
      placeholder="Describe the effect and cooldown..."
    />
  </G.HFlex>
);

const AbilitiesDisplay = (props: Abilities) => (
  <G.VFlex>
    <AbilityInput {...props.combat} />
    <AbilityInput {...props.movement} />
    <AbilityInput {...props.support} />
  </G.VFlex>
);

interface AttunementDisplayProps {
  attunements: Perk[];
}

const AttunementDisplay = (props: AttunementDisplayProps) => (
  <PerkTable
    title="Attunements"
    headings={{ type: "Level gained" }}
    perks={props.attunements}
    emptyMessage="No attunements"
    perkTypes={{
      optional: [],
      strict: attunementLabels(8),
    }}
  />
);

const attunementLabels = (numAttunements: number) =>
  Array(numAttunements)
    .fill(0)
    .map(
      (_, i) =>
        `Level ${120 + i * 60} ${i + 1 === numAttunements ? "(Final) " : ""
        }Evolution`
    );

const styleIf = (condition: boolean | undefined, style: string) =>
  condition ? style : "";

export default styleIf;

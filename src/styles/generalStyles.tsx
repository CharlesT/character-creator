import styled, { css } from "styled-components";
import { colors } from "../themes";

export const VFlex = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;

export const HFlex = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export const DataArea = styled(VFlex)`
  align-items: flex-start;
  justify-content: flex-start;
`;

export const MajorHeading = styled.h1`
    margin: 0;
    margin-bottom: 0.5em;
    text-align: center;
  `,
  MinorHeading = styled.h4`
    margin: 0;
    text-align: center;
  `,
  Text = styled.p`
    margin: 0;
  `;

export const kbdFocusOutline = css`
  :focus {
    outline: none;
  }

  :focus-visible {
    outline: 3px solid ${colors.teal.solidBg};
    outline-offset: 3px;
  }
`;

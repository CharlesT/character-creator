const VALUES_EssenceType = [
  "Basic",
  "Greater",
  "Celestial",
  "Immortal",
] as const;

export type EssenceType = typeof VALUES_EssenceType[number];

export const essenceTypeValues = VALUES_EssenceType.map(
  (val) => val as EssenceType
);

const VALUES_Attribute = [
  "Strength",
  "Dexterity",
  "Vitality",
  "Endurance",
  "Intelligence",
  "Wisdom",
  "All",
] as const;

export type Attribute = typeof VALUES_Attribute[number];

export const attributeValues = VALUES_Attribute.map((val) => val as Attribute);

const VALUES_BonusType = [
  "+ after base",
  "% after base",
  "+ to base",
  "% to base",
  "% of bonus as base",
] as const;

export type BonusType = typeof VALUES_BonusType[number];

export const bonusTypeValues = VALUES_BonusType.map((val) => val as BonusType);

const VALUES_Stage = [
  "Early Mortal",
  "Mid Mortal",
  "Peak Mortal",
  "Early Foundation",
  "Mid Foundation",
  "Peak Foundation",
  "Early Lord",
  "Mid Lord",
  "Peak Lord",
  "Early Monarch",
  "Mid Monarch",
  "Peak Monarch",
  "Early Heavenly",
  "Mid Heavenly",
  "Peak Heavenly",
  "Early Immortal",
  "Mid Immortal",
  "Peak Immortal",
  "Early Evolved",
  "Mid Evolved",
  "Peak Evolved",
  "Early Ascended",
  "Mid Ascended",
  "Peak Ascended",
  "Early Eternal",
  "Mid Eternal",
  "Peak Eternal",
] as const;

export type Stage = typeof VALUES_Stage[number];

export const stageValues = VALUES_Stage.map((val) => val as Stage);

const VALUES_Rarity = [
  "Common",
  "Uncommon",
  "Rare",
  "Epic",
  "Legendary",
  "Mythic",
  "Relic",
  "Masterwork",
  "Eternal",
] as const;

export type Rarity = typeof VALUES_Rarity[number];

export const rarityValues = VALUES_Rarity.map((val) => val as Rarity);

const VALUES_CoreSize = [
  "Base",
  "Small",
  "Medium",
  "Large",
  "Huge",
  "Stupendous",
  "Chungus",
] as const;

export type CoreSize = typeof VALUES_CoreSize[number];

export const coreSizeValues = VALUES_CoreSize.map((val) => val as CoreSize);

const VALUES_ConduitSpeed = [
  "Base",
  "Crawling",
  "Slow",
  "Average",
  "Fast",
  "Very fast",
  "Faster than a hedgehog",
] as const;

export type ConduitSpeed = typeof VALUES_ConduitSpeed[number];

export const conduitSpeedValues = VALUES_ConduitSpeed.map(
  (val) => val as ConduitSpeed
);

const VALUES_Tier = [1, 2, 3, 4, 5, 6, 7, 8, 9] as const;

export type Tier = typeof VALUES_Tier[number];

export const tierValues = VALUES_Tier.map((val) => val as Tier);

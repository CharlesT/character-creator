import styled from "styled-components";
import Label from "./basics/inputs/Label";
import TextInput from "./basics/inputs/TextInput";
import * as G from "../styles/generalStyles";
import { Rarity, rarityValues, Stage } from "../enums";
import { useState } from "react";
import Select from "./basics/menus/Select";
import TextEditor from "./TextEditor";
import Button from "./basics/inputs/Button";

interface Technique {
  name: string;
  description: string;
}

export interface Path {
  name: string;
  description: string;
  stage: Stage;
  rarity: Rarity;
  techniques: {
    base: Technique;
    branch: Technique;
    fruit: Technique;
  };
}

export const PathDisplay = (props: Path) => {
  return (
    <DisplayCard>
      <TextInput value={props.name} large minimal center fill />
      <G.HFlex style={{ justifyContent: "center", alignItems: "stretch" }}>
        <InfoColumn style={{ marginRight: "3em" }}>
          <SpacedLabel label="Stage" above>
            <G.MinorHeading>{props.stage}</G.MinorHeading>
          </SpacedLabel>
          <LabelledEnum
            label="Rarity"
            value={props.rarity}
            values={rarityValues}
          />
          <TextEditor
            label="Description"
            value={props.description}
            placeholder="What's the theme of the path?"
          />
        </InfoColumn>
        <InfoColumn>
          <TechniqueInput label="Base Technique" tech={props.techniques.base} />
          <TechniqueInput
            label="Branch Technique"
            tech={props.techniques.branch}
          />
          <TechniqueInput
            label="Fruit Technique"
            tech={props.techniques.fruit}
          />
        </InfoColumn>
      </G.HFlex>
    </DisplayCard>
  );
};

const InfoColumn = styled(G.VFlex)`
  width: 18em;
  justify-content: flex-start;
  align-items: stretch;
  margin: 1em;
`;

const DisplayCard = styled(G.VFlex)``;

const SpacedLabel = styled(Label)`
  margin: 0 0 1em 0;
  align-items: flex-start;
  width: 100%;
`;

const LabelledEnum = <T extends string>(props: {
  label: string;
  value: T;
  values: T[];
}) => {
  const [val, setVal] = useState(props.value);
  return (
    <SpacedLabel label={props.label} above>
      <Select
        currName={val}
        values={props.values.map((value) => ({
          name: value,
          description: "",
        }))}
        onSelect={(value) => setVal(value)}
      />
    </SpacedLabel>
  );
};

interface TechniqueInputProps {
  label: string;
  tech: Technique;
}

const TechniqueInput = (props: TechniqueInputProps) => (
  <SpacedLabel key={props.label} label={props.label} above>
    <G.HFlex>
      <TextInput value={props.tech.name} fill />
      <Button boxed text="D" />
    </G.HFlex>
  </SpacedLabel>
);

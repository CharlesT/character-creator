import styled from "styled-components";
import { colors } from "../themes";
import Label from "./basics/inputs/Label";
import clsx from "clsx";
import { BubbleMenu, Editor, EditorContent, useEditor } from "@tiptap/react";
import StarterKit from "@tiptap/starter-kit";
import { Toggle, ToggleGroup } from "./basics/inputs/ToggleGroup";
import Underline from "@tiptap/extension-underline";
import Typography from "@tiptap/extension-typography";
import Placeholder from "@tiptap/extension-placeholder";

const StyledEditable = styled(EditorContent)`
  height: 100%;
  width: 100%;

  .editor-content {
    margin: 0;
    outline: none;
  }

  .editor {
    transition: box-shadow 150ms;
    outline: none;
    border: none;
    border-radius: 0.2em;
    height: 100%;
    width: 100%;

    /* Treat the padding + box-shadow as an outline. */
    padding: 0.4em;
    margin: -0.4em;

    &.labelled {
      box-shadow: 0 0 0 2px ${colors.grey.subtleBorder};
    }

    :hover {
      box-shadow: 0 0 0 2px ${colors.grey.elementBorder};
    }
    :focus {
      box-shadow: 0 0 0 2px ${colors.grey.hoveredElementBorder};
    }
  }

  p.is-editor-empty:first-child::before {
    color: ${colors.grey.textLowContrast};
    content: attr(data-placeholder);
    float: left;
    height: 0;
    pointer-events: none;
  }
`;

interface TextEditorProps {
  label?: string;
  placeholder?: string;
  value: string;
}

export default function TextEditor(props: TextEditorProps) {
  // TODO: Add global setting for this.
  const isSpellCheckOn = false;

  const editor = useEditor({
    editorProps: {
      attributes: {
        class: clsx("editor", props.label && "labelled"),
      },
    },
    extensions: [
      StarterKit.configure({
        paragraph: {
          HTMLAttributes: {
            class: "editor-content",
          },
        },
        heading: false,
      }),
      Underline,
      Typography,
      Placeholder.configure({
        placeholder: props.placeholder ? props.placeholder : "type here...",
      }),
    ],
    content: props.value,
  });

  const textArea = (
    <>
      {editor && <Menu editor={editor} />}
      <StyledEditable
        spellCheck={isSpellCheckOn}
        className={clsx({ labelled: props.label })}
        editor={editor}
      />
    </>
  );

  return props.label ? (
    <TextEditorLabel label={props.label} above solid>
      {textArea}
    </TextEditorLabel>
  ) : (
    textArea
  );
}

const TextEditorLabel = styled(Label)`
  flex-grow: 1;
`;

interface MenuProps {
  editor: Editor;
}

const Menu = (props: MenuProps) => (
  <BubbleMenu editor={props.editor}>
    <ToggleGroup>
      <Toggle
        icon="bold"
        onPress={() => props.editor.chain().focus().toggleBold().run()}
        pressed={props.editor.isActive("bold")}
      />
      <Toggle
        icon="italic"
        onPress={() => props.editor.chain().focus().toggleItalic().run()}
        pressed={props.editor.isActive("italic")}
      />
      <Toggle
        icon="underline"
        onPress={() => props.editor.chain().focus().toggleUnderline().run()}
        pressed={props.editor.isActive("underline")}
      />
      <Toggle
        icon="strikethrough"
        onPress={() => props.editor.chain().focus().toggleStrike().run()}
        pressed={props.editor.isActive("strike")}
      />
    </ToggleGroup>
  </BubbleMenu>
);

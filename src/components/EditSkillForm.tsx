import { useState } from "react";
import styled from "styled-components";
import * as G from "../styles/generalStyles";
import Dialog from "./basics/Dialog";
import Button from "./basics/inputs/Button";
import Label from "./basics/inputs/Label";
import TextInput from "./basics/inputs/TextInput";

// TODO: Refactor into separate file.
export interface Skill {
  tier: number;
  rarity: string;
  description: string;
  history: SingleSkill | FullSkill;
}

interface SingleSkill {
  __typename: "SingleSkill";
  t1?: string;
  t2?: string;
  t3?: string;
  t4?: string;
  t5?: string;
  t6?: string;
  t7?: string;
}

export interface FullSkill {
  __typename: "FullSkill";
  leftT7: SingleSkill;
  rightT7: SingleSkill;
  t8?: string;
  t9?: string;
}

export const skillCase = <T extends unknown>(
  skill: Skill,
  // Object param `cases` is used so that the cases can have named parameters when func
  // is called, for clarity.
  cases: {
    ifSingleSkill: (s: SingleSkill) => T;
    ifFullSkill: (s: FullSkill) => T;
  }
): T => {
  switch (skill.history.__typename) {
    case "SingleSkill":
      return cases.ifSingleSkill(skill.history as SingleSkill);
    case "FullSkill":
      return cases.ifFullSkill(skill.history as FullSkill);
  }
};

/**/
/* Assumes that solo skills are only filled out on the left. This should work, but... */
/* TODO: Refactor this please, typewise.
 */
export const getCurrentSkillName = (skill: Skill): string =>
  skillCase(skill, {
    ifSingleSkill: (h) =>
      h.t7
        ? h.t7
        : h.t6
          ? h.t6
          : h.t5
            ? h.t5
            : h.t4
              ? h.t4
              : h.t3
                ? h.t3
                : h.t2
                  ? h.t2
                  : h.t1
                    ? h.t1
                    : "Invalid Skill",
    ifFullSkill: (h) => (h.t9 ? h.t9 : h.t8 ? h.t8 : "Missing combined name"),
  });

export default function EditSkillForm(props: {
  skill: Skill;
  isOpen: boolean;
  closeDialog: () => void;
}) {
  const [skill, setSkill] = useState(props.skill);

  // TODO: Refactor this, perhaps as a calculated field when
  // state management is added.
  const name = getCurrentSkillName(props.skill);

  return (
    <Dialog isOpen={props.isOpen} close={props.closeDialog}>
      <SkillDataEntry>
        <h3>Skill History ({name})</h3>
        <LabeledText label="Rarity" text={skill.rarity} />
        {skillCase(skill, {
          ifSingleSkill: (history) => (
            <G.HFlex>
              <UpToT7 tiers={history} />
            </G.HFlex>
          ),
          ifFullSkill: (history) => (
            <G.VFlex>
              <G.HFlex>
                <UpToT7 title="Skill 1" tiers={history.leftT7} />
                <div style={{ width: "2em" }} />
                <UpToT7 title="Skill 2" tiers={history.rightT7} />
              </G.HFlex>
              <h4>Final Tiers</h4>
              <LabeledText label="Tier 8" text={history.t8} />
              <LabeledText label="Tier 9" text={history.t9} />
            </G.VFlex>
          ),
        })}
        <G.HFlex
          style={{ justifyContent: "space-between", alignSelf: "stretch" }}
        >
          {skill.history.__typename === "SingleSkill" && (
            <Button
              text="Add second skill"
              onClick={() =>
                setSkill((currSkill) => ({
                  ...currSkill,
                  history: {
                    __typename: "FullSkill",
                    leftT7: currSkill.history as SingleSkill,
                    rightT7: { __typename: "SingleSkill" },
                  },
                }))
              }
            />
          )}

          <div />
          <Button text="Update Skill" onClick={props.closeDialog} />
        </G.HFlex>
      </SkillDataEntry>
    </Dialog>
  );
}

const LabeledText = (props: { label: string; text: string | undefined }) => (
  <Box>
    <Label label={props.label}>
      <TextInput value={props.text ? props.text : ""} />
    </Label>
  </Box>
);

const Box = styled.div`
  margin-bottom: 0.5em;
`;

const UpToT7 = (props: { title?: string; tiers: SingleSkill }) => (
  <G.VFlex>
    {props.title !== undefined && <h4>{props.title}</h4>}
    <LabeledText label="Tier 1" text={props.tiers.t1} />
    <LabeledText label="Tier 2" text={props.tiers.t2} />
    <LabeledText label="Tier 3" text={props.tiers.t3} />
    <LabeledText label="Tier 4" text={props.tiers.t4} />
    <LabeledText label="Tier 5" text={props.tiers.t5} />
    <LabeledText label="Tier 6" text={props.tiers.t6} />
    <LabeledText label="Tier 7" text={props.tiers.t7} />
  </G.VFlex>
);

const SkillDataEntry = styled(G.VFlex)`
  padding: 1em 3em 1em 3em;
  justify-content: space-around;
`;

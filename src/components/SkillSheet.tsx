import clsx from "clsx";
import { useState } from "react";
import styled from "styled-components";
import * as G from "../styles/generalStyles";
import { colors } from "../themes";
import Card from "./basics/Card";
import EditSkillForm, {
  getCurrentSkillName,
  Skill,
  skillCase,
} from "./EditSkillForm";

interface SkillSheetProps {
  actives: Skill[];
  passives: Skill[];
}

// TODO: Clean up the styling here. A lot of re-use.

export default function SkillSheet(props: SkillSheetProps) {
  return (
    <Card>
      <G.HFlex>
        <SkillList heading="Passive Skills" skills={props.passives} />
        <SkillList heading="Active Skills" skills={props.actives} />
      </G.HFlex>
    </Card>
  );
}

const SkillCard = styled(Card)`
  background-color: ${colors.grey.subtleBg};
  width: 35em;
  height: 6em;
  padding: 0;
  margin: 0 1.5em 1.2em 1.5em;

  display: flex;
  justify-content: flex-end;
  align-items: stretch;
  flex-direction: column;
`;

const SkillName = styled.h3<{ tier: number }>`
  margin: 0;
  color: ${(props) =>
    props.tier === 5
      ? colors.rarity.t5
      : props.tier === 6
        ? colors.rarity.t6
        : props.tier === 7
          ? colors.rarity.t7
          : props.tier === 8
            ? colors.rarity.t8
            : props.tier === 9
              ? colors.rarity.t9
              : colors.grey.textHighContrast};
`;

const SkillVariants = styled(G.HFlex)`
  align-items: stretch;
  justify-content: space-evenly;
  margin: 0;

  &.hasVariants {
    border-top: 1px solid ${colors.grey.hoveredElementBorder};
  }
`;

const Variant = styled.p`
  padding: 0.2em;
  margin: 0;
  font-size: 0.7em;
  text-align: center;
`;

const TopSpacing = styled(G.VFlex)`
  justify-content: center;
  flex-grow: 1;
`;

const SkillList = (props: { heading: string; skills: Skill[] }) => (
  <G.VFlex>
    <h1>{props.heading}</h1>
    {props.skills.map((skill: Skill, i) => (
      <MySkillCard skill={skill} position={i} />
    ))}
  </G.VFlex>
);

const BadgeArea = styled(G.HFlex)`
  position: relative;
  align-items: flex-start;
`;

const MySkillCard = (props: { skill: Skill; position: number }) => {
  const [open, setOpen] = useState(false);

  const name = getCurrentSkillName(props.skill);

  const badge = getRarityBadge(props.skill.rarity);

  return (
    <>
      <SkillCard
        sharp
        interactive
        key={props.position}
        onClick={() => {
          // Fixes bug where pressing a button in the dialog doesn't actually close it,
          // since clicking anything in the dialog counts as clicking the card, for
          // some reason...
          if (!open) setOpen(true);
        }}
      >
        {badge && <BadgeArea>{badge}</BadgeArea>}
        <TopSpacing>
          <SkillName tier={props.skill.tier}>{name}</SkillName>
        </TopSpacing>
        <SkillData skill={props.skill} />
      </SkillCard>
      <EditSkillForm
        skill={props.skill}
        isOpen={open}
        closeDialog={() => setOpen(false)}
      />
    </>
  );
};

const SkillData = (props: { skill: Skill }) => {
  const variants = getVariants(props.skill);

  return (
    <SkillVariants className={clsx({ hasVariants: variants.length > 0 })}>
      {variants.map(([variant, tier]) => (
        <Variant>
          <SkillName key={tier} tier={tier} as="p">
            {variant}
          </SkillName>
        </Variant>
      ))}
    </SkillVariants>
  );
};

const Badge = styled(G.HFlex)`
  justify-content: center;
  position: absolute;
  margin: 0.4em;
  font-weight: bold;
  font-size: 0.9em;
`;

const getRarityBadge = (rarity: string): JSX.Element | undefined => {
  const badge = (text: string) => <Badge>{text}</Badge>;

  switch (rarity) {
    case "Common":
      return badge("C");
    case "Uncommon":
      return badge("U");
    case "Rare":
      return badge("R");
    case "Epic":
      return badge("E");
    case "Legendary":
      return badge("L");
    case "Mythic":
      return badge("My");
    case "Relic":
      return badge("Re");
    case "Masterwork":
      return badge("Ma");
    case "Eternal":
      return badge("Et");
    default:
      return undefined;
  }
};

const getVariants = (skill: Skill): [string, number][] => {
  const { tier } = skill;

  return skillCase(skill, {
    ifFullSkill: (h) =>
      tier === 9
        ? []
        : // Typecasting because typescript isn't smart enough to realise that
        // this filter call removes all undefined values.
        ([
          [h.leftT7.t7, 7],
          [h.rightT7.t7, 7],
        ].filter((val) => val !== undefined) as [string, number][]),
    ifSingleSkill: (h) =>
      tier === 7
        ? []
        : ([h.t1, h.t2, h.t3, h.t4, h.t5, h.t6, h.t7]
          .slice(0, tier - 1)
          .map((val, i) => [val, i + 1]) // Need to remember tier, right now.
          .filter(([val, _]) => val) as [string, number][]),
  });
};

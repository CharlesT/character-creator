import { useEffect, useState } from "react";
import { Outlet } from "react-router-dom";
import styled, { createGlobalStyle } from "styled-components";
import { colors, themes } from "../themes";
import HeaderBar from "./HeaderBar";
import NavBar from "./NavBar";
import clsx from "clsx";
import * as G from "../styles/generalStyles";

// TODO: Find a better solution for sizing 'MySheet' if possible.
const headerHeight = "4em";

const Grid = styled.div`
  background-color: ${colors.grey.elementBg};
  height: 100vh;
  width: 100vw;
  display: grid;
  grid:
    [header-start] "nav header" ${headerHeight} [header-end]
    [content-start] "nav content" 1fr [content-end]
    / 17em 1fr;
  overflow-y: hidden;
`;

const MySheet = styled(G.VFlex)`
  grid-area: content;
  place-self: start start;

  width: 100%;
  height: calc(100vh - ${headerHeight});
  position: relative;
  background-color: ${colors.grey.appBg};
  border-top-left-radius: 10px;

  &::after {
    content: "";
    position: absolute;
    bottom: 0;
    right: 0;
    top: 0;
    left: 0;
    border-top-left-radius: 10px;
    box-shadow: ${colors.shadows.contentArea} inset;
    background-color: transparent;
    pointer-events: none;
  }
`;

const ScrollablePage = styled.div`
  /* This is needed so that the content doesn't stretch past the edge of the screen. */
  box-sizing: border-box;
  overflow-y: scroll;
  scrollbar-color: ${colors.grey.solidBg} transparent;
  width: 100%;
  max-height: 100%;
  border-radius: 20px;

  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  padding: 2em;
`;

const GlobalStyle = createGlobalStyle`
  * {
    color: ${colors.grey.textHighContrast};
    font-family: 'Montserrat', sans-serif;
  }

  /* For animating dark mode :D gonna wait though*/
  /* .changing-theme {
    transition: background-color 5s ease,
                border-color 5s ease,
                color 5s ease;
  }
 */

  :root {
    ${themes};
  }
`;

export default function AppLayout() {
  const [darkMode, setDarkMode] = useState(false);

  // TODO: Fix up this hack so that the body's class doesn't need to be edited for a theme change.
  useEffect(() => {
    document.body.className = darkMode ? "dark-theme" : "light-theme";
  }, [darkMode]);

  const toggleDarkMode = () => {
    setDarkMode(!darkMode);
  };

  return (
    <>
      <GlobalStyle />
      <Grid className={clsx(darkMode ? "dark-theme" : "light-theme")}>
        <HeaderBar darkMode={darkMode} toggleDarkMode={toggleDarkMode} />
        <NavBar />

        <MySheet>
          <ScrollablePage>
            <Outlet />
          </ScrollablePage>
        </MySheet>
      </Grid>
    </>
  );
}

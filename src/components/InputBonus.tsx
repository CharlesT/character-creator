import { useState } from "react";
import styled from "styled-components";
import Select from "./basics/menus/Select";
import * as G from "../styles/generalStyles";
import NumericInput from "./basics/inputs/NumericInput";
import {
  Attribute,
  attributeValues,
  BonusType,
  bonusTypeValues,
} from "../enums";

// TODO: Add this back
// const renderStatType = (statType: StatType): string => {
//   switch (statType) {
//     case StatType.PERC:
//       return "%";
//     case StatType.STATIC:
//       return "+";
//     case StatType.BASE:
//       return "_+";
//     case StatType.BASEPERC:
//       return "_%";
//   }
// };

const NumbersBox = styled(G.HFlex)`
  width: 25em;
  margin-bottom: 1em;
  align-items: stretch;

  /* Ensure borders do not overlap, looking ugly. */
  button {
    margin-left: -2px !important;
  }

  button,
  input {
    :hover {
      z-index: 1;
    }
  }
`;

export interface Bonus {
  value: number;
  attribute: Attribute;
  type: BonusType;
}

interface BonusProps {
  bonus: Bonus;
  types?: BonusType[];
}

// TODO: Fix border styling in groups
export default function InputBonus(props: BonusProps) {
  const [value, setValue] = useState(props.bonus.value);
  const [attr, setAttr] = useState(props.bonus.attribute);
  const [type, setType] = useState(props.bonus.type);
  return (
    <NumbersBox>
      <NumericInput value={value} setVal={setValue} />
      <Select
        values={attributeValues.map((t, i) => ({
          id: i,
          name: t,
          description: "",
        }))}
        currName={attr}
        onSelect={setAttr}
      />
      <Select
        values={(props.types ? props.types : bonusTypeValues).map((t, i) => ({
          id: i,
          name: t,
          description: "",
        }))}
        currName={type}
        onSelect={setType}
      />
    </NumbersBox>
  );
}

import { useLocation, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { colors } from "../themes";
import * as G from "../styles/generalStyles";
import clsx from "clsx";
import Icon from "./basics/inputs/Icon";

const Nav = styled(G.VFlex)`
  /* box-shadow: rgba(0, 0, 0, 0.1) -25px 0px 9px -25px inset; */
  grid-area: nav;
  background-color: ${colors.grey.elementBg};
  height: max(100vh, 100%);
`;

const Navigation = styled(G.VFlex)`
  align-self: stretch;
  align-items: stretch;
`;

const CenterText = styled.div`
  text-align: center;
  margin-bottom: 2em;
`;

const LogoBox = styled.div`
  width: 80%;
  margin: 2em 0 2em 0;
`

const NavBar = () => (
    <Nav>
        <LogoBox>
            <Icon icon="ghost" colour={colors.teal.solidBg} />
        </LogoBox>
        <CenterText>
            <G.MajorHeading>Custom Character Creator</G.MajorHeading>
        </CenterText>
        <Navigation>
            <NavButton path="/summary" label="SUMMARY" />
            <NavButton path="/class" label="CLASS" />
            <NavButton path="/cultivation" label="CULT" />
            <NavButton path="/skills" label="SKILLS" />
            <NavButton path="/bonusperks" label="BONUS PERKS" />
            <NavButton path="/titles" label="TITLES" />
        </Navigation>
    </Nav>
);

const ListItem = styled(G.VFlex)`
  background-color: transparent;
  border: 0;
  border-top: 1px solid ${colors.grey.subtleBorder};
  border-bottom: 1px solid ${colors.grey.subtleBorder};
  height: 3.2em;
  margin-bottom: 0.8em;
  justify-content: center;

  text-transform: uppercase;

  &.active {
    background-color: ${colors.teal.solidBg};
    border-color: ${colors.teal.solidBg};
    * {
      color: ${colors.teal.textHighContrast};
    }
  }

  :not(&.active) {
    :hover {
      transition: all 0.25s ease;
      cursor: pointer;
      * {
        color: ${colors.teal.textLowContrast};
      }
      background-color: ${colors.teal.hoveredElementBg};
      border-color: ${colors.teal.hoveredElementBorder};
    }
  }

  ${G.kbdFocusOutline};
`;

interface ItemProps {
    label: string;
    path: string;
    isActive?: boolean;
}

const NavButton = (props: ItemProps) => {
    const navigate = useNavigate();
    const { pathname } = useLocation(); // TODO: Find out if this is inefficient
    const goToPath = () => navigate(props.path);

    return (
        <ListItem
            as="button"
            onClick={goToPath}
            className={clsx({ active: pathname === props.path })}
        >
            <G.MinorHeading>{props.label}</G.MinorHeading>
        </ListItem>
    );
};

export default NavBar;

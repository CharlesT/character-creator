import * as TogglePrimitive from "@radix-ui/react-toggle";
import * as G from "../../../styles/generalStyles";
import styled from "styled-components";
import Button from "./Button";
import { IconName } from "./Icon";
import React from "react";
import { colors } from "../../../themes";
import clsx from "clsx";

export interface ToggleProps {
  icon: IconName;
  pressed: boolean;
  onPress: () => void;
}

export const Toggle = (props: ToggleProps) => (
  <TogglePrimitive.Root asChild>
    <TealButton
      className={clsx({ pressed: props.pressed })}
      text=""
      icon={props.icon}
      onClick={props.onPress}
    />
  </TogglePrimitive.Root>
);

export interface ToggleGroupProps {
  className?: string;
  children?: JSX.Element[];
}

export const ToggleGroup = React.forwardRef(
  (props: ToggleGroupProps, ref: React.ForwardedRef<HTMLDivElement>) => (
    <HorizontalGroup ref={ref} className={props.className}>
      {props.children}
    </HorizontalGroup>
  )
);

const HorizontalGroup = styled(G.HFlex)`
  border-radius: 5px;
  background-color: ${colors.teal.elementBg};
  box-shadow: ${colors.shadows.strong};
`;

const TealButton = styled(Button)`
  border-radius: none;
  border: none;
  padding: 0.4em !important;
  background-color: ${colors.teal.elementBg};
  * {
    color: ${colors.teal.textLowContrast};
  }

  :hover {
    background-color: ${colors.teal.hoveredElementBg} !important;
  }
  :active {
    background-color: ${colors.teal.activeElementBg} !important;
  }

  &.pressed {
    background-color: ${colors.teal.solidBg} !important;
    * {
      color: ${colors.teal.textHighContrast};
    }
  }
`;

import clsx from "clsx";
import React from "react";
import styled from "styled-components";
import { kbdFocusOutline } from "../../../styles/generalStyles";
import { colors } from "../../../themes";
import Icon, { IconName } from "./Icon";

interface ButtonProps {
  text: string;
  onClick?: () => void;
  icon?: IconName;
  boxed?: boolean;
  className?: string;
}

const Button = React.forwardRef<HTMLButtonElement, ButtonProps>(
  (props, ref) => {
    return (
      <StyledButton
        ref={ref} // Ref needed for radix-ui's 'asChild' API.
        onClick={props.onClick}
        className={clsx(props.className, {
          boxed: props.boxed,
          icon: props.icon,
        })}
      >
        {props.icon ? <Icon icon={props.icon} /> : <Text>{props.text}</Text>}
      </StyledButton>
    );
  }
);

const Text = styled.p`
  margin: 0;
`;

const StyledButton = styled.button`
  background-color: ${colors.grey.hoveredElementBg};
  border: 2px solid ${colors.grey.hoveredElementBorder};
  border-radius: 0.7em;
  padding: 0.5em;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: border-color 100ms ease;
  transition: background-color 100ms ease;

  :hover {
    cursor: pointer;
    border-color: ${colors.teal.hoveredElementBorder};
    background-color: ${colors.teal.hoveredElementBg};
  }

  :active {
    background-color: ${colors.teal.activeElementBg};
  }

  ${kbdFocusOutline};

  &.boxed {
    padding: 0.2em;
    border-radius: 0;
  }
  &.icon {
    padding: 0.3em;
    margin: 0;
    background-color: transparent;
    border: none;
    border-radius: 5px;
    :hover {
      background-color: ${colors.grey.elementBorder};
    }
    :active {
      background-color: ${colors.grey.hoveredElementBorder};
    }
  }
`;

export default Button;

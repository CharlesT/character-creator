import styled from "styled-components";
import { colors } from "../../../themes";
import { StyledTextInput } from "./TextInput";

const StyledNumeric = styled(StyledTextInput)`
  width: 4em;
  margin: 0;
  border-color: ${colors.grey.hoveredElementBorder};

  /* Remove spinners */
  -moz-appearance: textfield;

  &-webkit-outer-spin-button,
  &-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`;

interface NumericInputProps {
  value: number;
  setVal: (val: number) => void;
}

const NumericInput = (props: NumericInputProps) => {
  const changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newVal = Number(event.target.value);
    if (!isNaN(newVal)) props.setVal(newVal);
  };

  return (
    <StyledNumeric type="number" value={props.value} onChange={changeHandler} />
  );
};

export default NumericInput;

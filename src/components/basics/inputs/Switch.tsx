import * as SwitchPrimitive from "@radix-ui/react-switch";
import styled from "styled-components";
import { kbdFocusOutline } from "../../../styles/generalStyles";
import { colors } from "../../../themes";
import styleIf from "../../../utils";

interface SwitchProps {
  active: boolean;
  onClick?: () => void;
}

const Switch = (props: SwitchProps) => (
  <Root
    className={`${styleIf(props.active, "active")}`}
    onClick={props.onClick}
  >
    <Thumb />
  </Root>
);

const Root = styled(SwitchPrimitive.Root)`
  all: unset;
  width: 1.7em;
  height: 1em;
  border-radius: 9999px;
  box-shadow: 0 0 3px ${colors.grey.solidBg};
  background-color: ${colors.grey.textLowContrast};
  display: flex;

  &.active {
    background-color: ${colors.teal.solidBg};
    justify-content: flex-end;
  }

  :hover {
    cursor: pointer;
  }

  ${kbdFocusOutline};
`;

const Thumb = styled(SwitchPrimitive.Thumb)`
  width: 1em;
  height: 1em;
  border-radius: 9999px;
  background-color: white;
`;

export default Switch;

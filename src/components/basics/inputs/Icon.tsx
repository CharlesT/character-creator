import {
    FaBold,
    FaCog,
    FaItalic,
    FaMoon,
    FaPlus,
    FaPuzzlePiece,
    FaStrikethrough,
    FaSun,
    FaUnderline,
    FaBars,
    FaGhost,
} from "react-icons/fa";
import styled, { css } from "styled-components";
import { colors } from "../../../themes";

export type IconName =
    | "cog"
    | "moon"
    | "sun"
    | "bold"
    | "italic"
    | "underline"
    | "strikethrough"
    | "plus"
    | "bars"
    | "ghost";

interface IconProps {
    icon: IconName;
    colour?: string;
}

const styleSVG = css`
  width: 100%;
  height: 100%;
`;

const Icon = (props: IconProps) => {
    switch (props.icon) {
        case "cog":
            return (
                <Cog fill={props.colour ? props.colour : colors.grey.textLowContrast} />
            );
        case "moon":
            return <Moon />;
        case "sun":
            return <Sun />;
        case "bold":
            return <Bold />;
        case "italic":
            return <Italic />;
        case "underline":
            return <Underline />;
        case "strikethrough":
            return <Strikethrough />;
        case "plus":
            return <Plus />;
        case "bars":
            return <Bars />;
        case "ghost":
            return (
                <Ghost fill={props.colour ? props.colour : colors.grey.textLowContrast} />
            );
        default:
            return <PuzzlePiece />;
    }
};

const [Cog, Moon, Sun, PuzzlePiece, Bold, Italic, Underline, Strikethrough, Plus, Bars, Ghost] = [
    FaCog,
    FaMoon,
    FaSun,
    FaPuzzlePiece,
    FaBold,
    FaItalic,
    FaUnderline,
    FaStrikethrough,
    FaPlus,
    FaBars,
    FaGhost
].map(
    (icon) =>
        styled(icon)`
      ${styleSVG}
    `
);

export default Icon;

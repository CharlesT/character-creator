import clsx from "clsx";
import { useState } from "react";
import styled from "styled-components";
import { colors } from "../../../themes";

export const StyledTextInput = styled.input`
  background: transparent;
  width: 12em;
  border: 2px solid ${colors.grey.elementBorder};
  border-radius: 0;
  padding: 0.2em;
  margin-right: 0.5em;
  * {
    padding: 0;
  }

  &.large {
    font-size: 2em;
    font-style: bold;
  }

  &.center {
    text-align: center;
  }

  &.minimal {
    border: 2px solid transparent;
  }

  &.fill {
    width: unset;
    flex-grow: 1;
  }

  :hover {
    border-color: ${colors.teal.hoveredElementBorder};
    transition: box-shadow 200ms ease;
  }

  :focus {
    box-shadow: inset 0 0 3px 1px ${colors.teal.elementBorder};
    border-color: ${colors.teal.solidBg};
    outline: none;
  }
`;

const TextInput = (props: {
  value: string;
  large?: boolean;
  center?: boolean;
  minimal?: boolean;
  fill?: boolean;
}) => {
  const [val, setVal] = useState(props.value);
  return (
    <StyledTextInput
      className={clsx({
        large: props.large,
        center: props.center,
        minimal: props.minimal,
        fill: props.fill,
      })}
      type="text-input"
      value={val}
      onChange={(e) => setVal(e.target.value)}
    />
  );
};

export default TextInput;

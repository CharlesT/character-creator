import * as LabelPrimitive from "@radix-ui/react-label";
import clsx from "clsx";
import { PropsWithChildren } from "react";
import styled from "styled-components";
import * as G from "../../../styles/generalStyles";
import { colors } from "../../../themes";

interface LabelProps {
  label: string;
  labelWidth?: string;
  above?: boolean;
  solid?: boolean;
  className?: string;
  onClick?: () => void;
}

const Label = (props: PropsWithChildren<LabelProps>) => {
  const Box = props.above ? G.VFlex : G.HFlex;

  return (
    <Box className={props.className} onClick={props.onClick}>
      <StyledLabel
        className={clsx({ above: props.above, solid: props.solid })}
        width={props.labelWidth}
      >
        {props.label ? props.label : "Missing label"}
      </StyledLabel>
      {props.children}
    </Box>
  );
};

const StyledLabel = styled(LabelPrimitive.Root) <{ width?: string }>`
  width: ${(p) => (p.width ? p.width : "4em")};
  font-weight: bold;
  user-select: none;
  margin-right: 0.5em;
  text-align: right;
  z-index: 1;

  &.above {
    font-size: 0.7em;
    width: unset;
    align-self: flex-start;
    margin-bottom: 0.2em;
  }

  &.solid {
    margin-left: -0.2em;
    padding: 0 0.2em 0 0.2em;
    background-color: ${colors.grey.appBg};
  }
`;

export default Label;

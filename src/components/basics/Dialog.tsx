import * as DialogPrimitive from "@radix-ui/react-dialog";
import { PropsWithChildren } from "react";
import styled from "styled-components";
import { colors } from "../../themes";
import Card from "./Card";

interface DialogProps {
  isOpen: boolean;
  close: () => void;
}

const Dialog = (props: PropsWithChildren<DialogProps>) => (
  <StyledRoot open={props.isOpen}>
    <StyledOverlay />
    <StyledContent
      onEscapeKeyDown={props.close}
      onInteractOutside={props.close}
      onPointerDownOutside={props.close}
    >
      <Card>{props.children}</Card>
    </StyledContent>
  </StyledRoot>
);

export default Dialog;

const StyledRoot = styled(DialogPrimitive.Root)``;

const StyledOverlay = styled(DialogPrimitive.Overlay)`
  position: fixed;
  inset: 0;
  background-color: black;
  opacity: 0.6;
  z-index: 2;
`;

const StyledContent = styled(DialogPrimitive.Content)`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;

  filter: ${colors.shadows.dropShadow};
`;

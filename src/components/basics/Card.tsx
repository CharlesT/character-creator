import clsx from "clsx";
import { PropsWithChildren } from "react";
import styled from "styled-components";
import { kbdFocusOutline } from "../../styles/generalStyles";
import { colors } from "../../themes";

interface CardProps {
    className?: string; // For passing down styles
    sharp?: boolean;
    interactive?: boolean;
    onClick?: () => void;
}

const Card = (props: PropsWithChildren<CardProps>) => (
    <StyledCard
        as={props.interactive ? "button" : "div"}
        onClick={props.onClick}
        className={clsx(props.className, {
            sharp: props.sharp,
            interactive: props.interactive,
        })}
    >
        {props.children}
    </StyledCard>
);

export const StyledCard = styled.div`
  border: none;
  background-color: ${colors.grey.appBg};
  margin-bottom: 2em;
  padding: 1em;
  border-radius: 10px;
  border: 1px solid ${colors.grey.subtleBorder};
  box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;;

  .sharp {
    padding: 0;
    border-radius: 0;
    background-color: ${colors.grey.subtleBg};
    border: 1px solid ${colors.grey.elementBorder};
  }

  .interactive {
    :hover,
    :hover:focus {
      box-shadow: ${colors.shadows.strong};
      transition: box-shadow 100ms linear;
      outline: 2px solid ${colors.teal.hoveredElementBorder};
      cursor: pointer;
    }

    :hover:active {
      box-shadow: none;
      outline: 2px solid ${colors.teal.solidBg};
    }

    ${kbdFocusOutline};
  }
`;

export default Card;

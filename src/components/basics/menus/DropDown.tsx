import * as DropDownMenu from "@radix-ui/react-dropdown-menu";
import { PropsWithChildren } from "react";
import styled from "styled-components";
import { kbdFocusOutline } from "../../../styles/generalStyles";
import { colors } from "../../../themes";
import Icon, { IconName } from "../inputs/Icon";
import Switch from "../inputs/Switch";

interface DropDownProps {
  renderButton: JSX.Element;
}

export const DropDown = (props: PropsWithChildren<DropDownProps>) => {
  return (
    <DropDownMenu.Root>
      <Trigger asChild>{props.renderButton}</Trigger>
      <Content portalled>
        <Arrow offset={10} />
        {props.children}
      </Content>
    </DropDownMenu.Root>
  );
};

const Trigger = styled(DropDownMenu.Trigger)`
  border: none;
  background-color: transparent;
  width: 2em;
  height: 2em;
  border-radius: 5px;

  :hover {
    cursor: pointer;
    background-color: ${colors.grey.activeElementBg};
  }

  ${kbdFocusOutline};
`;

const Arrow = styled(DropDownMenu.Arrow)`
  fill: ${colors.grey.elementBorder};
`;

const Content = styled(DropDownMenu.Content)`
  background-color: ${colors.grey.elementBg};
  padding: 0.3em;
  cursor: pointer;
  border: 1px solid ${colors.grey.elementBorder};

  width: 12em;

  box-shadow: ${colors.shadows.strong};
`;

interface DropDownItemProps {
  onClick: () => void;
  icon?: IconName;
  switch?: {
    active: boolean;
  };
}

export const DropDownItem = (props: PropsWithChildren<DropDownItemProps>) => (
  <Item onClick={props.onClick}>
    {props.icon && (
      <IconBox>
        <Icon icon={props.icon} />
      </IconBox>
    )}
    <div style={{ flexGrow: 1 }}>{props.children}</div>
    {props.switch && <Switch active={props.switch.active} />}
  </Item>
);

const Item = styled(DropDownMenu.Item)`
  margin: 0;
  margin-bottom: 0.3em;
  padding: 0.1em;

  display: flex;
  justify-content: flex-start;
  align-items: center;

  :hover {
  }
`;

const IconBox = styled.div`
  height: 1em;
  margin-right: 0.4em;
`;

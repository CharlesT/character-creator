import * as DropDownMenu from "@radix-ui/react-dropdown-menu";
import { PropsWithChildren } from "react";
import styled from "styled-components";
import { colors } from "../../../themes";
import Button from "../inputs/Button";

export interface Selection<T extends string> {
  name: T;
  description: string;
}

interface SelectProps<T extends string> {
  values: Selection<T>[];
  currName: T;
  onSelect: (val: T) => void;
}

const Select = <T extends string>(props: PropsWithChildren<SelectProps<T>>) => {
  return (
    <DropDownMenu.Root modal={false}>
      <DropDownMenu.Trigger asChild>
        {props.children ? (
          props.children
        ) : (
          <Button boxed text={props.currName} />
        )}
      </DropDownMenu.Trigger>
      <Content portalled>
        <Arrow />
        {props.values.map(({ name, description }) => (
          <Item key={name} onSelect={() => props.onSelect(name)}>
            {name}
            <Spacer />
            <FadedText>{description}</FadedText>
          </Item>
        ))}
      </Content>
    </DropDownMenu.Root>
  );
};

export const makeSelections = <T extends string>(values: T[]): Selection<T>[] =>
  values.map((val) => ({
    name: val,
    description: "",
  }));

const Arrow = styled(DropDownMenu.Arrow)`
  fill: ${colors.teal.hoveredElementBorder};
`;

const Content = styled(DropDownMenu.Content)`
  border: 2px solid ${colors.teal.hoveredElementBorder};
  background: ${colors.grey.elementBg};
  box-shadow: ${colors.shadows.strong};
  width: 12em;
  border-radius: 0.3em;
  cursor: pointer;
`;

const Item = styled(DropDownMenu.Item)`
  padding: 0.5em;
  border-radius: 0.3em;
  :hover {
    background-color: ${colors.teal.hoveredElementBg};
    outline: 1px solid ${colors.teal.hoveredElementBorder};
  }

  display: flex;
`;

const Spacer = styled.div`
  flex-grow: 1;
`;

const FadedText = styled.span`
  color: ${colors.grey.solidBg};
`;

export default Select;

import styled from "styled-components";
import { colors } from "../themes";
import InputBonus, { Bonus } from "./InputBonus";
import TextEditor from "./TextEditor";
import Button from "./basics/inputs/Button";
import Card from "./basics/Card";
import * as G from "../styles/generalStyles";
import { BonusType } from "../enums";
import { useEffect, useMemo, useState } from "react";
import Select, { makeSelections, Selection } from "./basics/menus/Select";
import { Stack, List } from "immutable";

const Table = styled.table`
  border-collapse: collapse;
  background-color: ${colors.grey.appBg};

  td,
  thead {
    padding: 1em;
    text-align: left;
    vertical-align: top;
  }

  thead {
    border-bottom: 1px solid ${colors.grey.elementBorder};
  }
  tfoot {
    border-top: 1px solid ${colors.grey.elementBorder};
  }
`;

const PerkCard = styled(Card)`
  width: 90em;
`;

export interface Perk {
  type: string;
  name: string;
  description: string;
  bonuses: Bonus[];
}

interface PerkTableProps {
  title?: string;
  headings: {
    type?: string;
    name?: string;
    description?: string;
    bonus?: string;
  };
  perkTypes: {
    optional: string[];
    strict: string[];
  };
  emptyMessage?: string;
  bonusTypes?: BonusType[];
  perks: Perk[];
}

export default function PerkTable(props: PerkTableProps) {
  const [perks, setPerks] = useState(List(props.perks));

  const [perkStack, setPerkStack] = useState(
    Stack(
      // Remove the perk types already used.
      props.perkTypes.strict.filter(
        (type) => !props.perks.some((perk) => perk.type === type)
      )
    )
  );

  const [types, setTypes] = useState<Selection<string>[]>([]);

  const optionalSelections = useMemo(
    () => makeSelections(props.perkTypes.optional),
    [props.perkTypes.optional]
  );

  useEffect(() => {
    const peek = perkStack.peek();

    if (perkStack.isEmpty()) {
      setTypes(optionalSelections);
    } else if (peek) {
      setTypes([{ name: peek, description: "(next)" }, ...optionalSelections]);
    }
  }, [perkStack, optionalSelections]);

  const { type, name, description, bonus } = props.headings;

  const addPerk = (type: string) => {
    if (type === perkStack.peek()) setPerkStack((stack) => stack.shift());
    console.log(perkStack, props.perkTypes.strict);

    setPerks((p) => p.push(emptyPerk(type)));
  };

  return (
    <PerkCard>
      <G.MajorHeading>
        {props.title ? props.title : "Perk Table"}
      </G.MajorHeading>
      {perks.isEmpty() ? (
        <EmptyPerkTable
          message={props.emptyMessage ? props.emptyMessage : "No perks"}
          possibleTypes={types.map((v) => v.name)}
          addPerk={addPerk}
        />
      ) : (
        <Table>
          <thead>
            <tr>
              <td>{type ? type : "Perk Type"}</td>
              <td>{name ? name : "Perk Name"}</td>
              <td>{description ? description : "Description"}</td>
              <td>{bonus ? bonus : "Bonus Attributes"}</td>
            </tr>
          </thead>
          <tbody>
            {perks.map((perk, i) => perkToRow(i, perk, props.bonusTypes))}
          </tbody>
          <tfoot>
            <tr>
              <td>
                <Select values={types} currName={""} onSelect={addPerk}>
                  <Button icon="plus" text="" />
                </Select>
              </td>
              <td />
              <td />
              <td />
            </tr>
          </tfoot>
        </Table>
      )}
    </PerkCard>
  );
}

const emptyPerk = (type: string): Perk => ({
  type: type,
  name: "",
  description: "",
  bonuses: [],
});

const Type = styled.td`
  width: 12%;
`;

const Name = styled.td`
  width: 15%;
`;

const Description = styled.td`
  width: 45%;
`;

const BonusBox = styled.div`
  width: 15%;
  display: flex;
  flex-direction: column;
`;

const Row = styled.tr<{ odd: boolean }>`
  background-color: ${(props) =>
    props.odd ? colors.grey.elementBg : colors.grey.appBg};
`;

const perkToRow = (position: number, perk: Perk, bonusTypes?: BonusType[]) => {
  return (
    <Row odd={position % 2 === 0} key={perk.type}>
      <Type>{perk.type}</Type>
      <Name>
        <TextEditor value={perk.name} placeholder="a fancy name" />
      </Name>
      <Description>
        <TextEditor
          value={perk.description}
          placeholder="requirement(s) and/or effects"
        />
      </Description>
      <td>
        <BonusBox>
          {perk.bonuses.map(({ attribute, value, type }, id) => (
            <InputBonus
              key={id}
              bonus={{
                value: value,
                attribute: attribute,
                type: type,
              }}
              types={bonusTypes}
            />
          ))}
        </BonusBox>
      </td>
    </Row>
  );
};

interface EmptyPerkTableProps {
  message: string;
  possibleTypes: string[];
  addPerk: (type: string) => void;
}

const EmptyPerkTable = (props: EmptyPerkTableProps) => {
  return (
    <G.VFlex>
      <G.MinorHeading>{props.message}</G.MinorHeading>
      <Select
        values={props.possibleTypes.map((type) => ({
          name: type,
          description: "",
        }))}
        currName={""}
        onSelect={props.addPerk}
      >
        <Button text="Add one" />
      </Select>
    </G.VFlex>
  );
};

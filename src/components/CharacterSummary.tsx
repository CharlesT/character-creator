import { useState } from "react";
import styled from "styled-components";
import Label from "./basics/inputs/Label";
import TextInput from "./basics/inputs/TextInput";
import * as G from "../styles/generalStyles";
import Card from "./basics/Card";
import Select from "./basics/menus/Select";
import NumericInput from "./basics/inputs/NumericInput";
import { EssenceType } from "../enums";

interface StatElement {
  name: string;
  value: number;
}

export interface Stats {
  strength: number;
  dexterity: number;
  vitality: number;
  endurance: number;
  intelligence: number;
  wisdom: number;
}

export interface CharProps {
  name: string;
  faction: string;
  race: string;
  origin: string;
  essence: number;
  essenceType: EssenceType;
  stats: Stats;
}

const InputRow = styled(G.HFlex)`
  margin-bottom: 1em;
`;

const InfoBox = styled.div`
  flex-direction: column;
  display: flex;
`;

interface InfoProps {
  children?: React.ReactNode;
  prompt: string;
  text?: string;
}

const BigLabeledInput = (props: InfoProps) => {
  return (
    <G.VFlex>
      <G.MinorHeading>{props.prompt}</G.MinorHeading>
      <TextInput center minimal value={props.text ? props.text : ""} large />
    </G.VFlex>
  );
};

const SmallLabeledInput = (props: InfoProps) => (
  <Label label={props.prompt}>
    <TextInput value={props.text ? props.text : ""} />
  </Label>
);

const StatCard = styled(Card)`
  margin: 0;
  padding: 0.3em;
  border-radius: 0.5em;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const StatGroup = styled.div`
  display: grid;
  grid: auto / repeat(6, 1fr);
  gap: 0.6em;
`;

const StatBox = (props: StatElement) => {
  return (
    <StatCard interactive>
      <G.MinorHeading>{props.name}</G.MinorHeading>
      <div style={{ height: "0.5em" }} />
      <G.MinorHeading>{props.value}</G.MinorHeading>
    </StatCard>
  );
};

// <BigLabeledInput prompt="Character Name" text={props.name} />
//
//
const HeadingsBox = styled(G.VFlex)`
  justify-content: space-between;
  align-self: stretch;
`;

const essences = [
  {
    name: "Basic",
    description: "\u00D710^0",
  },
  {
    name: "Greater",
    description: "\u00D710^3",
  },
  {
    name: "Celestial",
    description: "\u00D710^6",
  },
  {
    name: "Immortal",
    description: "\u00D710^9",
  },
];

export default function CharacterSummary(props: CharProps): JSX.Element {
  const [type, setType] = useState(props.essenceType.toString());
  return (
    <Card>
      <G.HFlex>
        <HeadingsBox>
          <BigLabeledInput prompt="Character Name" text={props.name} />
          <div style={{ flexGrow: 1 }} />
          <BigLabeledInput prompt="Faction" text={props.faction} />
        </HeadingsBox>
        <InfoBox>
          <InputRow>
            <SmallLabeledInput prompt="Race" text={props.race} />
            <SmallLabeledInput prompt="Origin" text={props.origin} />
          </InputRow>
          <InputRow>
            <SmallLabeledInput prompt="Essence">
              <NumericInput value={props.essence} setVal={() => { }} />
            </SmallLabeledInput>
            <Select values={essences} currName={type} onSelect={setType} />
          </InputRow>
          <StatGroup>
            <StatBox name="Strength" value={props.stats.strength} />
            <StatBox name="Dexterity" value={props.stats.dexterity} />
            <StatBox name="Vitality" value={props.stats.vitality} />
            <StatBox name="Endurance" value={props.stats.endurance} />
            <StatBox name="Intelligence" value={props.stats.intelligence} />
            <StatBox name="Wisdom" value={props.stats.wisdom} />
          </StatGroup>
        </InfoBox>
      </G.HFlex>
    </Card>
  );
}

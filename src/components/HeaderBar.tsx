import styled from "styled-components";
import { colors } from "../themes";
import Button from "./basics/inputs/Button";
import Label from "./basics/inputs/Label";
import Switch from "./basics/inputs/Switch";
import { DropDown, DropDownItem } from "./basics/menus/DropDown";
import Select from "./basics/menus/Select";

interface HeaderProps {
    darkMode: boolean;
    toggleDarkMode: () => void;
}

const Header = styled.div`
  background-color: ${colors.grey.elementBg};
  grid-area: header;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  padding: 1em;
`;

export default function HeaderBar(props: HeaderProps) {
    return (
        <Header>
            <HeaderLabel label="Character:" labelWidth="6em">
                <Select
                    values={[{ name: "My Custom Character", description: "" }]}
                    currName="My Custom Character"
                    onSelect={() => { }}
                />
            </HeaderLabel>
            <div style={{ flex: 1 }} />

            <HeaderLabel
                label="Dark Mode"
                labelWidth="6em"
                onClick={props.toggleDarkMode}
            >
                <Switch active={props.darkMode} />
            </HeaderLabel>
            <DropDown renderButton={<Button text="" icon="cog" />}>
                <DropDownItem onClick={() => { }}>Download</DropDownItem>
            </DropDown>
        </Header>
    );
}

const HeaderLabel = styled(Label)`
  margin-right: 1em;
`;

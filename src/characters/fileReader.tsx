import {
  attributeValues,
  bonusTypeValues,
  conduitSpeedValues,
  coreSizeValues,
  essenceTypeValues,
  rarityValues,
  stageValues,
  tierValues,
} from "../enums";

export const jsonToCharacter = (json: any) => {
  return {
    summary: {
      name: str(json.summary.name),
      faction: str(json.summary.faction),
      race: str(json.summary.race),
      origin: str(json.summary.origin),
      essence: num(json.summary.essence),
      essenceType: enumerable(json.summary.essenceType, essenceTypeValues),
      stats: {
        strength: num(json.summary.stats.strength),
        dexterity: num(json.summary.stats.dexterity),
        vitality: num(json.summary.stats.vitality),
        endurance: num(json.summary.stats.endurance),
        intelligence: num(json.summary.stats.intelligence),
        wisdom: num(json.summary.stats.wisdom),
      },
    },
    backstory: {
      background: str(json.backstory.background),
      buildSummary: str(json.backstory.buildSummary),
    },
    class: {
      summary: {
        name: str(json.class.summary.name),
        level: num(json.class.summary.level),
        rarity: enumerable(json.class.summary.rarity, rarityValues),
        description: str(json.class.summary.description),
        attrPrimary: enumerable(
          json.class.summary.attrPrimary,
          attributeValues
        ),
        attrSecondary: enumerable(
          json.class.summary.attrSecondary,
          attributeValues
        ),
      },
      abilities: {
        combat: ability(json.class.abilities.combat),
        movement: ability(json.class.abilities.movement),
        support: ability(json.class.abilities.support),
        extras: list(json.class.abilities.extras).map(ability),
      },
      perks: list(json.class.perks).map(perk),
      attunements: list(json.class.attunements).map(perk),
    },
    cult: {
      primaryPath: path(json.cult.primaryPath),
      secondPath: path(json.cult.secondPath),
      aspect1: str(json.cult.aspect1),
      aspect2: json.cult.aspect2, // not using 'str' since this one is optional
      coreSize: enumerable(json.cult.coreSize, coreSizeValues),
      conduitSpeed: enumerable(json.cult.conduitSpeed, conduitSpeedValues),
      bonus: num(json.cult.bonus),
      perks: list(json.cult.perks).map(perk),
    },
    skill: {
      passive: list(json.skill.passive).map(skillDetails),
      active: list(json.skill.active).map(skillDetails),
      perks: list(json.skill.perks).map(perk),
    },
    titles: list(json.titles).map(perk),
    bonusPerks: list(json.bonusPerks).map(perk),
    equipment: {
      itemSets: list(json.equipment.setBonuses).map((set) => ({
        name: str(set.name),
        bonus: bonus(set.bonus),
        items: list(set.items).map(item),
      })),
      nonSetItems: list(json.equipment.items).map(item),
      storageRings: {
        primary: str(json.equipment.storageRings.primary),
        alternate: str(json.equipment.storageRings.alternate),
      },
      awakenedObj: {
        name: str(json.equipment.awakenedObj.name),
        description: str(json.equipment.awakenedObj.name),
        stage: enumerable(json.equipment.awakenedObj.stage, tierValues),
        perks: list(json.equipment.awakenedObj.perks).map(perk),
      },
    },
  };
};

const item = (item: any) => ({
  name: str(item.name),
  description: str(item.description),
  rarity: enumerable(item.rarity, rarityValues),
  bonuses: list(item.bonuses).map(bonus),
});

const skillDetails = (skill: any) => ({
  tier: enumerable(skill.tier, tierValues),
  rarity: enumerable(skill.rarity, rarityValues),
  description: str(skill.description),
  history: skill.history.leftT7
    ? {
      // This needs to be cast as a string literal, as this attribute is later used
      // to differentiate between skill types later and is typed as such.
      // See 'EditSkillForm.tsx'.
      __typename: "FullSkill" as const,
      leftT7: tiers1to7(skill.history.leftT7),
      rightT7: tiers1to7(skill.history.rightT7),
      t8: str(skill.history.t8),
      t9: skill.history.t9, // optional,
    }
    : tiers1to7(skill.history),
});

const tiers1to7 = (obj: any) => ({
  __typename: "SingleSkill" as const,
  // Note all tiers are optional, so no 'str' call required
  t1: obj.t1,
  t2: obj.t2,
  t3: obj.t3,
  t4: obj.t4,
  t5: obj.t5,
  t6: obj.t6,
  t7: obj.t7,
});

const path = (obj: any) => ({
  name: str(obj.name),
  description: str(obj.description),
  stage: enumerable(obj.stage, stageValues),
  rarity: enumerable(obj.rarity, rarityValues),
  techniques: {
    base: {
      name: str(obj.techniques.base.name),
      description: str(obj.techniques.base.description),
    },
    branch: {
      name: str(obj.techniques.branch.name),
      description: str(obj.techniques.branch.description),
    },
    fruit: {
      name: str(obj.techniques.fruit.name),
      description: str(obj.techniques.fruit.description),
    },
  },
});

const ability = (ability: any) => ({
  name: str(ability.name),
  description: str(ability.description),
});

const bonus = (obj: any) => ({
  attribute: enumerable(obj.attribute, attributeValues),
  value: num(obj.value),
  type: enumerable(obj.type, bonusTypeValues),
});

const perk = (perk: any) => ({
  type: str(perk.type),
  name: str(perk.name),
  description: str(perk.description),
  bonuses: list(perk.bonuses).map(bonus),
});

const list = (arr: any[] | undefined): any[] => (arr === undefined ? [] : arr);

const num = (value: number | undefined): number => {
  if (value === undefined) {
    console.error("(Character file) A numerical field was missing.");
    return 0;
  } else {
    return value;
  }
};

const str = (value: string | undefined): string => {
  if (value === undefined) {
    console.error("(Character file) A string field was missing.");
    return "";
  } else {
    return value;
  }
};

/* Returns first enum value if none is found. */
const enumerable = <T extends string | number>(
  value: string | undefined,
  values: T[]
): T => {
  if (value === undefined) {
    console.error(`(Character file) A string (enum) field was missing.`);
    return values[0];
  } else if (values.find((val) => (val as string) === value) === undefined) {
    console.error(
      `(Character file) The string '${value}' is not a member of the field type`
    );
    return values[0];
  } else {
    return value as T;
  }
};

export default jsonToCharacter;

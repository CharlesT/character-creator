# Custom Character Creator

This project was inspired by Ivan Kal's story, [Infinite Realm](https://infinite-realm.ivan-kal.com/), and serves as a way to create new custom characters which fit within the canon of its world.

Please note that this project was under construction. While the UI is mostly complete, furthur functionality will likely never be added. A complete re-write is underway, as I used this project as a learning experience, trying different ways to use several unfamiliar technologies.

![App in light mode](./screen_light.png)
![App in dark mode](./screen_dark.png)

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
